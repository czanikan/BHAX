<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Gutenberg!</title>
        <keywordset>
            <keyword/>
        </keywordset>
        <cover>
            <para>
                Programozás tankönyvek rövid olvasónaplói.
            </para>
        </cover>
    </info>
    <section>
        <title>Programozási alapfogalmak</title>
        <para>
            <citation>PICI</citation>
        </para>
        <para> 
            A számítógépek programozására szolgáló programozási nyelveket, három típusba/szintbe soroljuk:
        </para>
        <para>
            <itemizedlist> 
                <listitem>
                    gépi nyelv (amin a számítógép beszél)
                </listitem>
                <listitem>
                    assembly nyelv (egyfajta átmenete a két másik szintnek)
                </listitem>
                <listitem>
                    magas szintű nyelv (ez már valamivel közelebb áll az általunk beszélt emberi nyelvhez)
                </listitem> 
            </itemizedlist>
        </para>
        <para>
            A magas szintű programozási nyelven megírt programokat forrásprogramnak vagy forrásszövegnek (vagy akár forráskódnak) 
            hívják. Ezt viszont nem írhatjuk meg akárhogy. Minden magas szintű programozási nyelvnek megvan a maga szintaktikája 
            (a nyelvre vonatkozó formai és "nyelvtani" megszoritások) melyeket be kell tartania a programozónak, különben a fordítás
            során szintaktikai hibát kapunk. Ezen túl vannak még úgynevezett szemantikai szabályok is, melyek a tartalmiés értelmezési
            szabályok együttese.
            Tehát egy magas szintű nyelvet a szintaktikai és a szemantikai szabályok határoznak meg, különböztetnek meg a többitől.
        </para>
        <para>
            Ha megírtuk a forrásprogramot, abból még nem lesz futtathó program, hiszen a processzur nem beszéli a magas szintű nyelveket,
            ezért a programunkat a processzor számára is értelmezhetővé kell tenni, azaz lefordítani. Itt lépnek be a fordítók, mely adott 
            magas szintű nyelvből képes futtatható állományt (tárgyprogramot) előállítani.
            Ehhez a következő lépéseket hajtja végre:
        </para>
        <para>
            <itemizedlist>
                <listitem>
                    lexikális elemzés (lexikális egységekre darabolja a forrásszöveget)
                </listitem>
                <listitem>
                    szintaktikai elemzés (szintaktikai megkötéseknek teljseülésének ellenőrzése)
                </listitem>
                <listitem>
                    szemantikai elemzés (szemantikai megkötéseknek teljseülésének ellenőrzése)
                </listitem>
                <listitem>
                    kódgenerálás (a forrásprogramból gépi nyelvű kód készítése)
                </listitem>
            </itemizedlist>
        </para>
        <para>
            Általában egy fordító program egy tetszőlege magas szintű nyelvről egy tetszőleges gépi nyelvre fordít.
            Ezen felől léteznek olyan nyelvek (pl.: C), melyek tartalmaznak nem nyelvi elemeket is. Ilyenkor előfordítóra 
            is szükségünk van.
        </para>
        <para>
            Minden programozási nyelv rendelkezik egy úgynevezett hivatkozási nyelvvel, mely a programnyelv szabványa 
            (itt kerülnek definiálásra a szemantikai és szintaktikai megszorítások). Ezen felül léteznek implementációk, mely 
            a megszorításokat változtatva és/vagy kiegészítve platformspecifikussá (platformhoz kötötté, azaz pl.: egy 
            processzor típust vagy egy operációs rendszert támogat, míg a többin nem képes a működésre) teszi a programot. 
            Bár vannak úgynevezett crossplatform (platformok közti átjárás) megoldások, a teljes átjárást máig nem sikerült megoldani.
        </para>
        <title>A programnyelvek (rövid) osztályozása</title>
        <para>
            Imperatív nyelvek:
        </para>
        <para>
            <itemizedlist>
                <listitem>Algoritmikus nyelv</listitem>
                <listitem>Utasítások sorozatából áll (a processzor sorrol sorra hajtja végre a parancsokat)</listitem>
                <listitem>Fő programozási eszköze a változó (adatok tárolására szolgál és rajta keresztül közvetlen elérhetőek a memória
                    egységek)
                </listitem>
                <listitem>Neumann-architektúra alapú</listitem>
                <listitem>Alcsoportjai:</listitem>
                    <itemizedlist>
                        <listitem>Eljárásorientált</listitem>
                        <listitem>Objektumorientált (ma már inkább ez az általános)</listitem>
                    </itemizedlist>
            </itemizedlist>
        </para>
        <para>
            Dekleratív nyelvek:
        </para>
        <para>
            <itemizedlist>
                <listitem>Nem algoritmikus</listitem>
                <listitem>A programozó csak a problémát veti fel, a megoldást a nyelv implementációi adják</listitem>
                <listitem>Korlátozott a memóriaműveletek lehetősége (szinte nincs is)</listitem>
                <listitem>Alcsoportjai:</listitem>
                    <itemizedlist>
                        <listitem>Funkcionális nyelvek</listitem>
                        <listitem>Logikai nyelvek</listitem>
                    </itemizedlist>
            </itemizedlist>
        </para>
        <title>Karakterkészlet</title>
        <para>
            Ahogyan az írott nyelvnek, úgy a programoknak is az alapkövei a karakterek. Tekinthetjük atomi egységeinek is, 
            hiszen nem tudjuk tovább bontani. Karakterekből épül fel minden. Viszont nem mindegy hogy milyen karaktert mikor és hol
            használunk, ugyanis vannak karakterek melyeket nem támogat egy nyelv, vannak speciális tulajdonsággal felruházott karakterek
            , stb. A félreértések elkerülése véget a karakterek alatt nem csak a betűket kell érteni, hanem:
        </para>
        <para>
            <itemizedlist>
                <listitem>betűk</listitem>
                <listitem>számok</listitem>
                <listitem>egyéb karakterek</listitem>
            </itemizedlist>
        </para>
        <para>
            A legtöbb nyelv nem támogatja a nemzeti nyelveket, kizárólag az angol abc betűit (általánosan csak a nagybetűket támogatja
            az összes nyelv, kisbetűk esetén már vannak eltérések és ezen felül is vannak, melyek nem különböztetik meg a kis- és 
            nagybetűket, míg mások igen)
        </para>
        <para>
            A számjegyek helyzete már jóval egyszerűbb, ugyanis a decimális számrendszer az általánosan elterjedt.
        </para>
        <para>
            Az egyéb karakterek közé tartoznak az elhatároló jelek, a műveleti jelek, az írásjelek és a speciális karakterek. Ezeknek
            általában külön szerepük van a programnyelvben.
        </para>
        <title>Lexikális egységek</title>
        <para>
            Röviden azok az elemei a forráslódnak melyet a fordító a lexikális elemzés során azonosít és tokenizál. Fajtái:
            <itemizedlist>
                <listitem>Többkarakteres szimbólum: karaktersorozat melynek az adott nyelv tulajdonít jelentést.
                    Gyakran elhatároló vagy operátori funkciót töltenek be.
                </listitem>
                <listitem>Szimbólikus nevek: lehet azonosító, kulcsszó vagy standard azonosító</listitem>
                <listitem>Címke: utasítások megjelölésére szolgál objektumorientált környezetben, későbbi hivatkozás céljából.</listitem>
                <listitem>Megjegyzés: megjegyzéseket írhatunk a kódunk későbbi olvasóinak (akár magunknak is) az egyszerűbb
                    értelmezés céljából. Ezeket a sorokat a fordító figyelmen kívűl hagyja.
                </listitem>
                <listitem>Literál: másnéven konstansok, tehát olyan értékek amik nem változnak</listitem>
            </itemizedlist>
        </para>
    </section>        
    <section>
        <title>Programozás bevezetés</title>
        <para>                
            <citation>KERNIGHANRITCHIE</citation>
        </para>
        <subtitle>Olvasónapló:</subtitle>
        <para>
            Ebben a részben egy rövid, de példaorientált részt kaptunk a C nyelv alapjairól.
            Felsorolt változó típusok, általános ciklusokat (while és for), argomentumokat. 
            Ami aránylag újdonság volt számomra az a karaktertömbök voltak. Már a turingos feladatok kidolgozása 
            során volt szerencsém megismerkedni ezzel a csodával. Én alapvetőleg C++-t tanultam ahol a sztringek 
            natívan voltak kezelve, így naív módon fel se merült bennem, hogy C-ben ez nem így van... Egészen az első 
            hibaüzenetig fordításkor. De ezen túl is rengeteg hasznos példakódot tartalmaz a könyv melyet később 
            kamatoztathatok.

            Részletes kifejtés következik... 
        </para>        
    </section>        
    <section>
        <title>Programozás</title>
        <para>                
            <citation>BMECPP</citation>
        </para>
        <title>C vs C++</title>
        <subtitle>Függvények visszatérési értéke és paramétere</subtitle>
        <para>
            Az egyik legszembetűnőbb különbség lehet, hogy míg a C-ben, ha paraméter nélkül definiálunk egy függvényt, akkor 
            tetszőleges mennyiségű paramétert hívhatunk meg:
        </para>
        <programlisting language="c"><![CDATA[
            void f()
            {
                //c függvény
            }
            ]]>
        </programlisting>
        <para>
            Ezzel szemben ugyanehhez a C++-ban egy speciális "void" paramétert kell megadnunk:
        </para>
        <programlisting language="c++"><![CDATA[
            void f(void)
            {
                //c++ függvény
            }
            ]]>
        </programlisting>
        <para>
            Ezen felül eltérés még, hogy ha nem adunk meg visszatérési értéket. C-ben van alapértelmezett érték, míg C++-ban 
            erre hibát fog visszadobni a fordító. 
        </para>
        <subtitle>A main() függvény</subtitle>
        <para>
            C++-ban kétféle módon hívhatjuk meg a main függvényt.
            Az első az általános, ami megegyezik a C-s verzióval is: 
        </para>
        <programlisting language="c++"><![CDATA[
            int main()
            {
                //please insert a program...
            }
            ]]>
        </programlisting>
        <para>
            És van a kicsit bonyolúltabb társa, ahol már paraméterezve van. Itt kap egy integer (egész szám) típusú argc-t, 
            mely a parancssori-argumentumok számát adja és egy char tömb típusú (karakter tömb, nevezhetnénk szövegnek is) 
            argv mely a paranccsori-argumentumokat tartalmazza.
        </para>
        <para>
            Ezzen felül nem kötelező megadni a main-be return értéket. Ha kihagyjuk akkor a fordító automatikusan return 0-ként 
            (sikeres futás) értelmezi. 
        </para>
        <subtitle>Bool típus</subtitle>
        <para>
            A C++ nyelvben további újdonság a C-hez képest, hogy van lehetőségünk boolen típusó (igaz/hamis) változókat 
            létrehozni, melyek a true vagy a false logikai értékeket vehetik fel. Ezekre a típusokra bool néven hivatkozhatunk. 
            Nyílván van lehetőség C-ben is logikai értékek tárolására (pl egy int-el ahol az 1 az igaz és a 0 a hamis), de ezek 
            kevésbé könnyen olvashatóak vagy értelmezhetőek.
        </para>
        <subtitle>Több-bájtos sztringek</subtitle>
        <para>
            A C nyelvben is van rá lehetőség, de használatukhoz meg kell hívnunk egy header-t (pl.: stdlib.h), ahol definiálva 
            van ez a típus. Ezzel szemben a C++ nyelvben már natív módon benne van és nem kell hozzá meghívni semmit. 
        </para>
        <subtitle>Változódeklaráció mint utasítás</subtitle>
        <para>
            C++ nyelven van lehetőségünk egy változót egy függvényen belül deklarálni és nem kell külön a main-ben, majd 
            utánna paraméterként meghívni. Nyílván ez azt is jelenti, hogy a változónk hatásköre csak az adott függvényre/ciklusra terjed ki
            ahol deklarálva lett.
        </para>
    </section>
    <section>
        <title>Python bevezető</title>
        <citation>BevMobProg</citation>
        <para>
            A python egy objektum orientált, platform független, magas szintű programozási nyelv.
            Gyakran javasolják kezdő programozóknak, aránylag egyszerű szintaxisa és rövid megoldásai miatt.
            Szerintem a legnagyobb előnye, hogy adott problémára a megoldást sokkal rövidebben és tömörebben lehet megoldani 
            így a kód maga is átláthatóbb lesz és nem ijeszti el a kezdőket mint mondjuk ugyan ez a megoldás java nyelven.
        </para>
        <subtitle>Szintaxis</subtitle>
        <para>
            Ha valaki programozott már valamilyen C nyelven (C, C++, C#) vagy esetleg java-n, akkor az első szembetűnő
            különbség, hogy a függvények hatáskörét nem kapcsos zárójelek határolják, hanem a behúzás mennyisége határozza 
            meg hogy az adott utasítás még ahhoz a függvényhez vagy ciklushoz tartozik-e vagy sem (viszont ciklusok és if-ek 
            után ":"-ot kell elhelyezni, de ez is inkább arra hasonlít mintha egy írott szövegben csinálnánk egy 
            felsorolást azokról az utasításokról, amiket végre kell hajtania a programunknak). Ezen túl nem kell utasítások 
            végére pontosveszőt (;) tenni. Ezen túl itt is van lehetőségünk sorokat megjegyzésnek megjelölni, ezeknek a soroknak az elejére 
            "#" karaktert kell elhelyeznünk, ha meg több sorral szeretnénk megcsinálni ugyan ezt, akkor van lehetőség több sor 
            kommentelésére is, az elején és a végén elhelyezett 3db aposztróf jellel (''')'. 
        </para>
        <subtitle>Típusok és változók</subtitle>
        <para>
            A változók típusa az megegyezik a legtöbb objektumorientált nyelvével (pl.: stringek, egészek, lebegőpontos, boolen,
            stb), viszont akik a C nyelvekhez van szokva azt meglepetésként érheti, hogy nem kell a változók típusát megadni 
            deklarációkor, hanem azt a fordító találja ki a megadott kezdőértékből (nyílván ebből következik az is, hogy muszáj
            kezdőértéket adni). Ez is nagyban kedvez a programozással ismerkedőknek. 
        </para>
        <subtitle>Változók és alkalmazásuk</subtitle>
        <para>
            Az előbb már beszéltünk kicsit a változókról, most viszont beszéljünk kicsit az objektum orientált megközelitésből is. 
            Van lehetőségünk globális és lokális változók létrehozására. C#-ban erre a public és private (meg a protected) kulcsszavakkal 
            hivatkozunk a változó deklaráció elején. Itt is hasonló a helyzet. Ha egy függvényen belül hozunk létre változót az automatikusan 
            lokális változó lesz, mely számunkra azt jelenti, hogy a hatásköre az adott függvényre vonatkozik. Tehát nem tudunk 
            se hivatkozni az értékére, se módosítani azt egy másik függvényből. Ha mégis szeretnénk akkor globálissá kell tennünk a változót, 
            tehát lényegében láthatóvá kell tenni. Ehhez nem kell mást tenni, mint a változó deklarációnál, a neve elé írni, hogy 
            'global'. Ugye, hogy nem is nehéz. 
        </para>
        <subtitle>A nyelv eszközei</subtitle>
        <para>
            <itemizedlist>
                <listitem>
                    Függvények: Megint csak találhatunk egy szintaktikai érdekességet, ugyanis a függvények esetén 
                    nem deklaráljuk a visszatérési érték típusát. Helyette a 'def' szócska szerepel, jelezvén hogy itt definiálunk 
                    valamit.
                </listitem>
                <listitem>
                    Ciklusok: ugyan úgy van for meg while ciklus, viszont nekem a legnagyobb megdöbbenésem az akkor jött, mikor a 
                    malmö feladat során rájöttem, hogy hoppá, nem tudok for ciklust írni. Rövid keresés során megtaláltam, hogy ehhez 
                    szükségem van benne egy extra Range() függvényre (pl.: for i in Range(10) ~ for(int i = 0; i < 10; i++)).
                </listitem>
                <listitem>
                    Elágazások: itt is működnek a már megszokott if/elif/else kifejezésék, az igazi újdonság, hogy a python a 
                    zárójelekkel is spórol, így az elágazás feltételét egyszerűen csak a kulcsszó után kell írni. 
                </listitem>
                <listitem>
                    Osztályok, objektumok: sajnos ezt minimálisan tárgyalta a könyv, pedig itt nyerne csak igazán értelmet az objektum 
                    orientáltság. Az alapvető lényeg az lenne, hogy class-eket tudjunk definiálni, melyek különböző tulajdonságokkal rendelkezhetnek 
                    így kicsit részletesebben modellezhetjük le a világunkat.
                </listitem>
            </itemizedlist>
        </para>
    </section>        
</chapter>                
